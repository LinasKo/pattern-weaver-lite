from pathlib import Path
import yaml
from typing import Tuple, Dict


def read_colors(yaml_in: Path) -> Dict[str, str]:
    with open(yaml_in, "r") as f:
        colors = yaml.safe_load(f.read())

    for name, value in colors.items():
        if not value:
            raise ValueError(
                f"Neranda spalvos '{name}' reikšmės. Ar pamiršai kabutes \" apie spalvos kodą, pvz \"#ffffff\"?")

    return colors


def make_img_stem(color_1: str, color_2: str) -> str:
    return f"{color_1} - {color_2}"


def split_img_stem(stem: str) -> Tuple[str, str]:
    split = stem.split(' - ')
    if len(split) != 2:
        raise ValueError(
            f"Stem '{stem}' is not a valid image stem. Should be 'color1 - color2' (color cannot have ' - ' in its name)")
    return split[0], split[1]
