"""
Step 1: Takes a folder of patterns and a list of colors
        Generates all combinations of patterns and colors
"""
from PIL import Image
from pathlib import Path
import yaml
from utils import read_colors, make_img_stem
from typing import Tuple, Dict


def is_within_tolerance(color, target, tolerance=10):
    return all(abs(c - t) <= tolerance for c, t in zip(color, target))


def gen_pattern(pattern_path: Path, vertical_color: str, horizontal_color: str, output_size: Tuple[int, int]) -> Image.Image:
    # Load the pattern image
    pattern = Image.open(pattern_path)
    pattern = pattern.convert("RGB")

    # Define colors
    vertical_color_rgb = tuple(
        int(vertical_color.lstrip('#')[i:i+2], 16) for i in (0, 2, 4))
    horizontal_color_rgb = tuple(
        int(horizontal_color.lstrip('#')[i:i+2], 16) for i in (0, 2, 4))

    # Process pixels
    for x in range(pattern.width):
        for y in range(pattern.height):
            current_color = pattern.getpixel((x, y))
            if is_within_tolerance(current_color, (255, 255, 255)):  # Off-white pixel
                pattern.putpixel((x, y), vertical_color_rgb)  # type: ignore
            elif is_within_tolerance(current_color, (0, 0, 0)):  # Off-black pixel
                pattern.putpixel((x, y), horizontal_color_rgb)  # type: ignore
            else:
                raise ValueError(
                    f"Unexpected color at pixel ({x}, {y}): {current_color}")

    # Repeat the pattern to fill output size
    scaled_pattern = Image.new("RGB", output_size)
    for x in range(0, output_size[0], pattern.width):
        for y in range(0, output_size[1], pattern.height):
            scaled_pattern.paste(pattern, (x, y))

    return scaled_pattern


def generate_all_combinations(out_dir: Path, patterns_folder: Path, colors: Dict[str, str], output_size: Tuple[int, int]):
    # Ensure output directory exists
    out_dir.mkdir(parents=True, exist_ok=True)

    # Iterate through all patterns and color combinations
    for pattern_file in patterns_folder.iterdir():
        if not pattern_file.is_file():
            print("Skipping", pattern_file, "because it is not a file")
            continue

        for vertical_color, vertical_color_hex in colors.items():
            for horizontal_color, horizontal_color_hex in colors.items():
                output_pattern = gen_pattern(
                    pattern_file, vertical_color_hex, horizontal_color_hex, output_size)

                # Create dir for pattern
                pattern_dir = out_dir / pattern_file.stem
                pattern_dir.mkdir(parents=True, exist_ok=True)

                # Define file names for output pattern and its reverse
                img_stem = make_img_stem(vertical_color, horizontal_color)
                out_file = pattern_dir / f"{img_stem}.png"

                # Save output pattern and its reverse
                output_pattern.save(out_file)


if __name__ == "__main__":
    PATTERN_DIR = Path("example/patterns")
    COLOR_FILE = Path("example/colors.yaml")
    OUT_SIZE_PX = (180, 180)
    OUT_DIR = Path("example/output")

    colors = read_colors(COLOR_FILE)

    generate_all_combinations(OUT_DIR, PATTERN_DIR, colors, OUT_SIZE_PX)
