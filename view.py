from flask import Flask, render_template, send_from_directory, request
from pathlib import Path
import math
from utils import read_colors, split_img_stem

app = Flask(__name__)

GALLERY = Path("example") / "output"
COLORS = Path("example") / "colors.yaml"
colors = read_colors(COLORS)


@app.route('/')
def index():
    patterns = [x.name for x in GALLERY.iterdir() if x.is_dir()]
    return render_template('index.html', patterns=patterns)


@app.route('/gallery')
def gallery():
    pattern_name = request.args.get('pattern', '')
    pattern_folder = GALLERY / pattern_name
    images = list(pattern_folder.glob("*.png"))
    grid_size = math.ceil(math.sqrt(len(images)))

    if int(grid_size) != grid_size:
        raise ValueError(
            f"Grid size is not a whole number: {grid_size} (len(images)={len(images)})")

    # Sort images based on color order in the YAML file
    color_order = list(colors.keys())  # List of color names in order
    images.sort(key=lambda img: (
        color_order.index(split_img_stem(img.stem)[0]),
        color_order.index(split_img_stem(img.stem)[1])
    ))

    return render_template('gallery.html', images=images, pattern_name=pattern_name, grid_size=grid_size)


@app.route('/pattern/<pattern_name>/<filename>')
def send_image(pattern_name, filename):
    return send_from_directory(GALLERY / pattern_name, filename)


if __name__ == "__main__":
    app.run(debug=True)
